'use strict';

var restful = require('node-restful'),
	mongoose = restful.mongoose,
	Schema = mongoose.Schema,
	timestamps = require('mongoose-timestamp');

var OrderSchema = new Schema({
	orderDate: {
		type: Date,
		required: true,
		default: Date.now
	},
	products: [{
		product: {
			type: String,
			ref: 'Product',
			required: true
		},
		portions: {
			type: Number,
			required: true
		},
		averagePortion: {
			type: Number,
			required: true
		},
		pricePerKg: {
			type: Number,
			required: true
		},
		amount: {
			type: Number,
			required: true
		},
	}]
});

// Auto add created & modified fields
OrderSchema.plugin(timestamps,  {
	createdAt: 'created',
	updatedAt: 'modified'
});

module.exports = restful.model('Order', OrderSchema);