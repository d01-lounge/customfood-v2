'use strict';

var restful = require('node-restful'),
	mongoose = restful.mongoose,
	Schema = mongoose.Schema,
	timestamps = require('mongoose-timestamp');

var UserSchema = new Schema({
	userId: {
		type: String
	},
	preferences: [{
		type: String,
		ref: 'Product'
	}]
});

// Auto add created & modified fields
UserSchema.plugin(timestamps,  {
	createdAt: 'created',
	updatedAt: 'modified'
});

module.exports = restful.model('User', UserSchema);