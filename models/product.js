'use strict';

var restful = require('node-restful'),
	mongoose = restful.mongoose,
	Schema = mongoose.Schema,
	timestamps = require('mongoose-timestamp');

var ProductSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	category: {
		type: String,
		enum: ['kazen', 'sauzen', 'vlees', 'salami']
	},
	priceKg: {
		type: Number,
		required: true
	},
	averagePortion: {
		type: Number,
		required: true
	}
});

// Auto add created & modified fields
ProductSchema.plugin(timestamps,  {
	createdAt: 'created',
	updatedAt: 'modified'
});

module.exports = restful.model('Product', ProductSchema);