'use strict';

var nodemailer = require('nodemailer');

var sendMail = function(to, subject, html, callback) {
	var transporter = nodemailer.createTransport({
		service: 'Gmail',
		auth: {
			user: 'user',
			pass: 'pass'
		}
	});

	var mailOptions = {
		from: 'Thomas Bormans <thomas.bormans@me.com>',
		to: to,
		subject: subject,
		html: html
	};

	transporter.sendMail(mailOptions, function(err, info){
		callback(err, info);
	});
};
module.exports.sendMail = sendMail;