'use strict';

var _ = require('lodash'),
	User = require('../models/user');

module.exports = _.extend(require('./restful')(User));