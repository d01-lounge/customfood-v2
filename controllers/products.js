'use strict';

var _ = require('lodash'),
	Product = require('../models/product');

module.exports = _.extend(require('./restful')(Product));