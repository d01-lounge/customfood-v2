'use strict';

// Update modified field for every PUT call
module.exports.modified = function(req, res, next) {
	req.body.modified = new Date();
	next();
};