'use strict';

module.exports = function(model){
	return {
		route : function(route, options){
			model.route(route, options);
		},
		methods : function(methods){
			model.methods(methods);
		},
		register : function(app, restfulPath){
			model.register(app, restfulPath);
		},
		before : function(method,action){
			model.before(method, action);
		},
		after : function(method, action){
			model.after(method, action);
		}
	};
};