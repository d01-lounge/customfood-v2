'use strict';

var _ = require('lodash'),
	Order = require('../models/order');

module.exports = _.extend(require('./restful')(Order));