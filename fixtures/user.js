var mongoose = require('mongoose'),
	ObjectId = mongoose.Types.ObjectId;

exports.User = {
	user1: {
		_id: new ObjectId('550adf3899fbe92a168d3051'),
		userId: "54eca4b04ebef6cdabf44ad1",
		preferences: [
			"5459f6a64de8c2e14612c251"
		]
	},
	user2: {
		_id: new ObjectId('550adf3899fbe92a168d3052'),
		userId: "54eca4b04ebef6cdabf44ad2",
		preferences: [
			"5459f6a64de8c2e14612c252", "5459f6a64de8c2e14612c253"
		]
	},
	user3: {
		_id: new ObjectId('550adf3899fbe92a168d3053'),
		userId: "54eca4b04ebef6cdabf44ad3",
		preferences: [
			"5459f6a64de8c2e14612c251", "5459f6a64de8c2e14612c253"
		]
	}
};
