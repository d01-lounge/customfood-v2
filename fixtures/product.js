var mongoose = require('mongoose'),
	ObjectId = mongoose.Types.ObjectId;

exports.Product = {
	product1: {
		"_id": new ObjectId("5459f6a64de8c2e14612c251"),
		"name": "Chamois d'Or",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product2: {
		"_id": new ObjectId("5459f6a64de8c2e14612c252"),
		"name": "Roquefort",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product3: {
		"_id": new ObjectId("5459f6a64de8c2e14612c253"),
		"name": "Petrella",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product4: {
		"_id": new ObjectId("5459f6a64de8c2e14612c254"),
		"name": "Chaumes",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product5: {
		"_id": new ObjectId("5459f6a64de8c2e14612c255"),
		"name": "Père Joseph",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product6: {
		"_id": new ObjectId("5459f6a64de8c2e14612c256"),
		"name": "SuprÃ¨me",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product7: {
		"_id": new ObjectId("5459f6a64de8c2e14612c257"),
		"name": "Port Salut",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product8: {
		"_id": new ObjectId("5459f6a64de8c2e14612c258"),
		"name": "Rambol met noten",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product9: {
		"_id": new ObjectId("5459f6a64de8c2e14612c259"),
		"name": "Rambol met kruiden",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product10: {
		"_id": new ObjectId("459f6a64de8c2e14612c2510"),
		"name": "Rubens",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product11: {
		"_id": new ObjectId("459f6a64de8c2e14612c2511"),
		"name": "Geitenkaas",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product12: {
		"_id": new ObjectId("459f6a64de8c2e14612c2512"),
		"name": "Franse brie",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product13: {
		"_id": new ObjectId("459f6a64de8c2e14612c2513"),
		"name": "Rustique Camembert",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product14: {
		"_id": new ObjectId("459f6a64de8c2e14612c2514"),
		"name": "Cambozola",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product15: {
		"_id": new ObjectId("459f6a64de8c2e14612c2515"),
		"name": "Leerdammer",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product16: {
		"_id": new ObjectId("459f6a64de8c2e14612c2516"),
		"name": "Maredsous",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product17: {
		"_id": new ObjectId("459f6a64de8c2e14612c2517"),
		"name": "Chester",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product18: {
		"_id": new ObjectId("459f6a64de8c2e14612c2518"),
		"name": "Gerookte kaas en hesp",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product19: {
		"_id": new ObjectId("459f6a64de8c2e14612c2519"),
		"name": "Platte kaas (mager)",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product20: {
		"_id": new ObjectId("459f6a64de8c2e14612c2520"),
		"name": "Platte kaas (vet)",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product21: {
		"_id": new ObjectId("459f6a64de8c2e14612c2521"),
		"name": "Platte kaas (aardbeien)",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product22: {
		"_id": new ObjectId("459f6a64de8c2e14612c2522"),
		"name": "Passendale",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product23: {
		"_id": new ObjectId("459f6a64de8c2e14612c2523"),
		"name": "Jonge kaas",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product24: {
		"_id": new ObjectId("459f6a64de8c2e14612c2524"),
		"name": "Oude kaas",
		"category": "kazen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product25: {
		"_id": new ObjectId("459f6a64de8c2e14612c2525"),
		"name": "Eiersla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product26: {
		"_id": new ObjectId("459f6a64de8c2e14612c2526"),
		"name": "Lentesla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product27: {
		"_id": new ObjectId("459f6a64de8c2e14612c2527"),
		"name": "Seldersla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product28: {
		"_id": new ObjectId("459f6a64de8c2e14612c2528"),
		"name": "Champignons Ã  la Grecque",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product29: {
		"_id": new ObjectId("459f6a64de8c2e14612c2529"),
		"name": "Komkommersla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product30: {
		"_id": new ObjectId("459f6a64de8c2e14612c2530"),
		"name": "Haring in dille",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product31: {
		"_id": new ObjectId("459f6a64de8c2e14612c2531"),
		"name": "Pikante tonijnsla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product32: {
		"_id": new ObjectId("459f6a64de8c2e14612c2532"),
		"name": "Garnaalsla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product33: {
		"_id": new ObjectId("459f6a64de8c2e14612c2533"),
		"name": "Tonijnsla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product34: {
		"_id": new ObjectId("459f6a64de8c2e14612c2534"),
		"name": "Haring in curry",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product35: {
		"_id": new ObjectId("459f6a64de8c2e14612c2535"),
		"name": "Martino prÃ©parÃ©",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product36: {
		"_id": new ObjectId("459f6a64de8c2e14612c2536"),
		"name": "Vleessla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product37: {
		"_id": new ObjectId("459f6a64de8c2e14612c2537"),
		"name": "Kippensla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product38: {
		"_id": new ObjectId("459f6a64de8c2e14612c2538"),
		"name": "Kip chop choy",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product39: {
		"_id": new ObjectId("459f6a64de8c2e14612c2539"),
		"name": "Pikante kip",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product40: {
		"_id": new ObjectId("459f6a64de8c2e14612c2540"),
		"name": "PrÃ©parÃ©",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product41: {
		"_id": new ObjectId("459f6a64de8c2e14612c2541"),
		"name": "PrÃ©pare met ui",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product42: {
		"_id": new ObjectId("459f6a64de8c2e14612c2542"),
		"name": "Balletjes in tomaat",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product43: {
		"_id": new ObjectId("459f6a64de8c2e14612c2543"),
		"name": "Hespensla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product44: {
		"_id": new ObjectId("459f6a64de8c2e14612c2544"),
		"name": "Ham met bieslooksla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product45: {
		"_id": new ObjectId("459f6a64de8c2e14612c2545"),
		"name": "Kip curry",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product46: {
		"_id": new ObjectId("459f6a64de8c2e14612c2546"),
		"name": "Krabsla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product47: {
		"_id": new ObjectId("459f6a64de8c2e14612c2547"),
		"name": "Mimosasla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product48: {
		"_id": new ObjectId("459f6a64de8c2e14612c2548"),
		"name": "Zalmsla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product49: {
		"_id": new ObjectId("459f6a64de8c2e14612c2549"),
		"name": "Vismimosa",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product50: {
		"_id": new ObjectId("459f6a64de8c2e14612c2550"),
		"name": "Napolitaanse sla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product51: {
		"_id": new ObjectId("459f6a64de8c2e14612c2551"),
		"name": "Gebraad in mosterdsla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product52: {
		"_id": new ObjectId("459f6a64de8c2e14612c2552"),
		"name": "Pittasla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product53: {
		"_id": new ObjectId("459f6a64de8c2e14612c2553"),
		"name": "Bouilliesla",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product54: {
		"_id": new ObjectId("459f6a64de8c2e14612c2554"),
		"name": "Kip hawaii",
		"category": "sauzen",
		"priceKg": 0,
		"averagePortion": 0
	},
	product55: {
		"_id": new ObjectId("459f6a64de8c2e14612c2555"),
		"name": "Gandaham",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product56: {
		"_id": new ObjectId("459f6a64de8c2e14612c2556"),
		"name": "Serranoham",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product57: {
		"_id": new ObjectId("459f6a64de8c2e14612c2557"),
		"name": "Vlaamse boerenham",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product58: {
		"_id": new ObjectId("459f6a64de8c2e14612c2558"),
		"name": "Zwarte woud ham",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product59: {
		"_id": new ObjectId("459f6a64de8c2e14612c2559"),
		"name": "Gerookte ham",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product60: {
		"_id": new ObjectId("459f6a64de8c2e14612c2560"),
		"name": "Rauwe ham",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product61: {
		"_id": new ObjectId("459f6a64de8c2e14612c2561"),
		"name": "Ardeense filet",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product62: {
		"_id": new ObjectId("459f6a64de8c2e14612c2562"),
		"name": "Filet de sax",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product63: {
		"_id": new ObjectId("459f6a64de8c2e14612c2563"),
		"name": "Gerookte kalkoen",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product64: {
		"_id": new ObjectId("459f6a64de8c2e14612c2564"),
		"name": "Varkenslever (sneetjes)",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product65: {
		"_id": new ObjectId("459f6a64de8c2e14612c2565"),
		"name": "Rosbief",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product66: {
		"_id": new ObjectId("459f6a64de8c2e14612c2566"),
		"name": "Ossegerookt",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product67: {
		"_id": new ObjectId("459f6a64de8c2e14612c2567"),
		"name": "Paardje (zout)",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product68: {
		"_id": new ObjectId("459f6a64de8c2e14612c2568"),
		"name": "Paardje (zoet)",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product69: {
		"_id": new ObjectId("459f6a64de8c2e14612c2569"),
		"name": "Leverworst",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product70: {
		"_id": new ObjectId("459f6a64de8c2e14612c2570"),
		"name": "BlokpatÃ©",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product71: {
		"_id": new ObjectId("459f6a64de8c2e14612c2571"),
		"name": "Peperbacon",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product72: {
		"_id": new ObjectId("459f6a64de8c2e14612c2572"),
		"name": "Breughelkop",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product73: {
		"_id": new ObjectId("459f6a64de8c2e14612c2573"),
		"name": "Zure frut",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product74: {
		"_id": new ObjectId("459f6a64de8c2e14612c2574"),
		"name": "Kalkoenspreskop zilveruitjes",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product75: {
		"_id": new ObjectId("459f6a64de8c2e14612c2575"),
		"name": "Varkensgebraad",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product76: {
		"_id": new ObjectId("459f6a64de8c2e14612c2576"),
		"name": "Filet de York",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product77: {
		"_id": new ObjectId("459f6a64de8c2e14612c2577"),
		"name": "Kasslerrib",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product78: {
		"_id": new ObjectId("459f6a64de8c2e14612c2578"),
		"name": "Corned Beaf",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product79: {
		"_id": new ObjectId("459f6a64de8c2e14612c2579"),
		"name": "Bacon",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product80: {
		"_id": new ObjectId("459f6a64de8c2e14612c2580"),
		"name": "Grillworst",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product81: {
		"_id": new ObjectId("459f6a64de8c2e14612c2581"),
		"name": "Fricandellen koek",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product82: {
		"_id": new ObjectId("459f6a64de8c2e14612c2582"),
		"name": "Kippenkoek",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product83: {
		"_id": new ObjectId("459f6a64de8c2e14612c2583"),
		"name": "Provençaalse koek",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product84: {
		"_id": new ObjectId("459f6a64de8c2e14612c2584"),
		"name": "Parijse worst",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product85: {
		"_id": new ObjectId("459f6a64de8c2e14612c2585"),
		"name": "Kalkoenworst",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product86: {
		"_id": new ObjectId("459f6a64de8c2e14612c2586"),
		"name": "Kippenwit",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product87: {
		"_id": new ObjectId("459f6a64de8c2e14612c2587"),
		"name": "Kalkoenwit",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product88: {
		"_id": new ObjectId("459f6a64de8c2e14612c2588"),
		"name": "Kippenwit met tuinkruiden",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product89: {
		"_id": new ObjectId("459f6a64de8c2e14612c2589"),
		"name": "Kip provençaals",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product90: {
		"_id": new ObjectId("459f6a64de8c2e14612c2590"),
		"name": "Boulet",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product91: {
		"_id": new ObjectId("459f6a64de8c2e14612c2591"),
		"name": "Hespenworst",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product92: {
		"_id": new ObjectId("459f6a64de8c2e14612c2592"),
		"name": "Strasbourg",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product93: {
		"_id": new ObjectId("459f6a64de8c2e14612c2593"),
		"name": "Kalfsworst",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product94: {
		"_id": new ObjectId("459f6a64de8c2e14612c2594"),
		"name": "Strasbourg met bieslook",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product95: {
		"_id": new ObjectId("459f6a64de8c2e14612c2595"),
		"name": "Goudhesp",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product96: {
		"_id": new ObjectId("459f6a64de8c2e14612c2596"),
		"name": "Beenhesp",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product97: {
		"_id": new ObjectId("459f6a64de8c2e14612c2597"),
		"name": "Natuurhesp",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product98: {
		"_id": new ObjectId("459f6a64de8c2e14612c2598"),
		"name": "Ontvette hesp",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product99: {
		"_id": new ObjectId("459f6a64de8c2e14612c2599"),
		"name": "Gekookte tong",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product100: {
		"_id": new ObjectId("59f6a64de8c2e14612c25100"),
		"name": "Kalfsrol met hersensaus",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product101: {
		"_id": new ObjectId("59f6a64de8c2e14612c25101"),
		"name": "Kalfsrol met tartaarsaus",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product102: {
		"_id": new ObjectId("59f6a64de8c2e14612c25102"),
		"name": "PatÃ© van de chef",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product103: {
		"_id": new ObjectId("59f6a64de8c2e14612c25103"),
		"name": "Smeerpaté",
		"category": "vlees",
		"priceKg": 0,
		"averagePortion": 0
	},
	product104: {
		"_id": new ObjectId("59f6a64de8c2e14612c25104"),
		"name": "Ringworst",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product105: {
		"_id": new ObjectId("59f6a64de8c2e14612c25105"),
		"name": "Salami",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product106: {
		"_id": new ObjectId("59f6a64de8c2e14612c25106"),
		"name": "Boerenring",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product107: {
		"_id": new ObjectId("59f6a64de8c2e14612c25107"),
		"name": "Salami met look",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product108: {
		"_id": new ObjectId("59f6a64de8c2e14612c25108"),
		"name": "Fijnkost",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product109: {
		"_id": new ObjectId("59f6a64de8c2e14612c25109"),
		"name": "Salam royal",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product110: {
		"_id": new ObjectId("59f6a64de8c2e14612c25110"),
		"name": "Peperblok",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product111: {
		"_id": new ObjectId("59f6a64de8c2e14612c25111"),
		"name": "Boulogne",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product112: {
		"_id": new ObjectId("59f6a64de8c2e14612c25112"),
		"name": "Bolzano",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product113: {
		"_id": new ObjectId("59f6a64de8c2e14612c25113"),
		"name": "Pain d'Ardenne",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product114: {
		"_id": new ObjectId("59f6a64de8c2e14612c25114"),
		"name": "Fôret d'Ardenne",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product115: {
		"_id": new ObjectId("59f6a64de8c2e14612c25115"),
		"name": "Chorizo",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product116: {
		"_id": new ObjectId("59f6a64de8c2e14612c25116"),
		"name": "Kalkoensalami",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product117: {
		"_id": new ObjectId("59f6a64de8c2e14612c25117"),
		"name": "Felino salami",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product118: {
		"_id": new ObjectId("59f6a64de8c2e14612c25118"),
		"name": "Chili salami",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product119: {
		"_id": new ObjectId("59f6a64de8c2e14612c25119"),
		"name": "Droge worstjes",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product120: {
		"_id": new ObjectId("59f6a64de8c2e14612c25120"),
		"name": "Serranosalami",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product121: {
		"_id": new ObjectId("59f6a64de8c2e14612c25121"),
		"name": "Salami truffel en parmezaan",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	},
	product122: {
		"_id": new ObjectId("59f6a64de8c2e14612c25122"),
		"name": "Salami provençaalse kruiden",
		"category": "salami",
		"priceKg": 0,
		"averagePortion": 0
	}
};