var mongoose = require('mongoose'),
	ObjectId = mongoose.Types.ObjectId;

exports.Order = {
	order1: {
		_id: new ObjectId('550adf3899fbe92a168d3051'),
		orderDate: new Date(),
		products: [{
			product: '5459f6a64de8c2e14612c251',
			portions: 2,
			pricePerKg: 1.2,
			averagePortion: 0.33,
			amount: 0.66
		}, {
			product: '5459f6a64de8c2e14612c252',
			portions: 3,
			pricePerKg: 2.4,
			averagePortion: 0.44,
			amount: 1.32
		}, {
			product: '5459f6a64de8c2e14612c253',
			portions: 1,
			pricePerKg: 3.6,
			averagePortion: 0.55,
			amount: 0.55
		}]
	},
	order2: {
		_id: new ObjectId('550adf3899fbe92a168d3052'),
		orderDate: new Date(),
		products: [{
			product: '5459f6a64de8c2e14612c251',
			portions: 4,
			pricePerKg: 1.2,
			averagePortion: 0.33,
			amount: 1.32
		}, {
			product: '5459f6a64de8c2e14612c252',
			portions: 1,
			pricePerKg: 2.4,
			averagePortion: 0.44,
			amount: 0.44
		}]
	}
};
