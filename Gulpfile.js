var gulp = require('gulp'),
	nodemon = require('gulp-nodemon'),
	jshint = require('gulp-jshint'),
	less = require('gulp-less'),
	stylish = require('jshint-stylish'),
	watch = require('gulp-watch'),
	LessPluginCleanCSS = require('less-plugin-clean-css'),
	LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	rename = require("gulp-rename"),
	path = require('path'),
	argv = require('yargs').argv,
	options = {
		'node': true,
		'strict': true,
		'undef': true,
		'unused': 'vars',
		'noempty': true,
		'latedef': true,
		'freeze': true,
		'forin': true,
		'curly': true,
		'eqeqeq': true,
		'bitwise': true,
		'sub': true,
		'predef': ['it', 'describe', 'before', 'beforeEach', 'after', 'afterEach']
	};

// LESS Plugins
var cleancss = new LessPluginCleanCSS({ advanced: true }),
	autoprefix= new LessPluginAutoPrefix({ browsers: ["last 2 versions"] });

// Scripts
gulp.task('lint', function () {
	gulp.src(['./models/**/*.js', './controllers/**/*.js', './routes/**/*.js', './fixtures/**/*.js'])
		.pipe(jshint(options))
		.pipe(jshint.reporter(stylish));
});

// Styles
gulp.task('less', function () {
	gulp.src('./public/assets/css/less/*.less')
		.pipe(less({
			paths: [ path.join(__dirname, 'less', 'includes') ],
			plugins: [autoprefix] // add 'cleancss' to array for minifying css
		}))
		.pipe(rename({
			basename: 'style',
			extname: '.css'
		}))
		.pipe(gulp.dest('./public/assets/css'));
});

// Nodemon
gulp.task('nodemon', function () {
	var env = argv.env || 'development';
	nodemon({ script: 'app.js', ext: 'js', env: {'NODE_ENV': env}, ignore: ['ignored.js'] })
		.on('change', ['lint'])
		.on('restart', function () {
			console.log('Server restarting now.');
		});
});

// Watch
gulp.task('watch', function () {
	// Watch .less files
	gulp.watch('./public/assets/css/less/*.less', ['less']);
});


gulp.task('dev', ['nodemon', 'watch']);
