'use strict';

var users = require('../controllers/users'),
	core = require('../controllers/core'),
	baseUrl = '/user';

module.exports = function(app) {
	users.methods(['get', 'post', 'put', 'delete']);
	users.before('put', core.modified);
	users.before('post', users.createUser);

	users.register(app, baseUrl);
};
