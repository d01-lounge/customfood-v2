'use strict';

var products = require('../controllers/products'),
	core = require('../controllers/core'),
	baseUrl = '/product';

module.exports = function(app) {
	products.methods(['get', 'post', 'put', 'delete']);
	products.before('put', core.modified);
	products.before('post', products.createUser);

	products.register(app, baseUrl);
};