'use strict';

var orders = require('../controllers/orders'),
	core = require('../controllers/core'),
	baseUrl = '/order';

module.exports = function(app) {
	orders.methods(['get', 'post', 'put', 'delete']);
	orders.before('put', core.modified);
	orders.before('post', orders.createUser);

	orders.register(app, baseUrl);
};