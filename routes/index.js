'use strict';

var baseUrl = '/';

module.exports = function(app) {
	app.route(baseUrl).get(function(req, res, next) {
		res.send('Hello World!');
	});
};
