#!/bin/sh
#start our app

g='\033[0;32m' # Green
r='\033[0;31m' # Red
lg='\033[1;32m' # Light Green
lga='\033[0;37m' # Light Gray
NC='\033[0m' # No Color

echo "${g}"
echo "           +---------------------------------------------------------------+"
echo "           |                  ${r}Installing NPM dependencies${g}                  |"
echo "           +---------------------------------------------------------------+"
echo "${lga}"
npm install
echo "${g}"
echo "           +---------------------------------------------------------------+"
echo "           |                   ${r}Running nodemon with gulp${g}                   |"
echo "           +---------------------------------------------------------------+"
echo "${lga}"
gulp dev