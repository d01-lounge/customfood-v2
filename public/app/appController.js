(function() {
    'use strict';

    angular
        .module('customFood.controllers')
        .controller('appController', appController);

    appController.$inject = ['$scope', '$mdSidenav'];

    /* @ngInject */
    function appController($scope, $mdSidenav) {
        var vm = this;
        vm.title = 'appController';

        /// $scope variables

        $scope.menuItems = [
            {
                id: 0,
                url: 'home',
                name: 'Home',
                icon: 'home.svg'
            },
            {
                id: 1,
                url: 'preferences',
                name: 'Voorkeuren',
                icon: 'star.svg'
            },
            {
                id: 2,
                url: 'products',
                name: 'Producten',
                icon: 'restaurant.svg'
            },
            {
                id: 3,
                url: 'orders',
                name: 'Bestellingen',
                icon: 'shopping-basket.svg'
            },
            {
                id: 4,
                url: 'faq',
                name: 'FAQ',
                icon: 'help.svg'
            }
        ];

        initialize();


        ////////////////

        $scope.toggle = function () {
            $mdSidenav('left').toggle();
        };

        function initialize() {
            console.info('Hello');
        }
    }
})();