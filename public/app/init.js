//Define a module that groups all filters
angular.module('customFood.filters', []);

//Define a module that groups all directives
angular.module('customFood.directives', []);

//Define a module that groups all factories
angular.module('customFood.factories', []);

//Define a module that groups all services, it has factories as a dependency
angular.module('customFood.services', ['customFood.factories']);

//Define a module that groups all controllers, it has services as a dependency
angular.module('customFood.controllers', ['customFood.services']);

//Define your app module.
angular.module('customFood', [
	'customFood.filters',
	'customFood.directives',
	'customFood.factories',
	'customFood.services',
	'customFood.controllers',
	'ui.router',
	'ngMaterial'
]);