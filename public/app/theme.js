angular
    .module('customFood')
    .config(theme);

theme.$inject =
    ['$mdThemingProvider'];

function theme ($mdThemingProvider) {

    runTheme();

    ////////////////

    function runTheme() {
        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('red');
    }
}