(function() {
    'use strict';

    angular
        .module('customFood')
        .controller('ordersController', ordersController);

    ordersController.$inject = ['$scope'];

    /* @ngInject */
    function ordersController($scope) {
        var vm = this;
        vm.title = 'ordersController';

        /// $scope variables

        initialize();


        ////////////////

        function initialize() {
        	console.info('Initialized ordersController');
        }
    }

})();