(function() {
    'use strict';

    angular
        .module('customFood')
        .controller('homeController', homeController);

    homeController.$inject = ['$scope'];

    /* @ngInject */
    function homeController($scope) {
        var vm = this;
        vm.title = 'homeController';

        /// $scope variables

        initialize();


        ////////////////

        function initialize() {
        	console.info('Initialized homeController');
        }
    }

})();