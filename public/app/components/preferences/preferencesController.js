(function() {
    'use strict';

    angular
        .module('customFood')
        .controller('preferencesController', preferencesController);

    preferencesController.$inject = ['$scope'];

    /* @ngInject */
    function preferencesController($scope) {
        var vm = this;
        vm.title = 'preferencesController';

        /// $scope variables

        initialize();


        ////////////////

        function initialize() {
        	console.info('Initialized preferencesController');
        }
    }

})();