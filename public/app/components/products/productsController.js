(function() {
    'use strict';

    angular
        .module('customFood')
        .controller('productsController', productsController);

    productsController.$inject = ['$scope'];

    /* @ngInject */
    function productsController($scope) {
        var vm = this;
        vm.title = 'productsController';

        /// $scope variables

        initialize();


        ////////////////

        function initialize() {
        	console.info('Initialized productsController');
        }
    }

})();