angular
    .module('customFood')
    .config(router);

router.$inject =
    ['$stateProvider', '$urlRouterProvider'];

function router ($stateProvider, $urlRouterProvider) {

    runRouter();

    ////////////////

    function runRouter() {

        $urlRouterProvider.otherwise('');

        $stateProvider

            // Home
            .state('home', {
                url: '/home',
                data: {
                    title: 'Home'
                },
                views: {
                    '': {
                        templateUrl: 'app/components/home/home.html',
                        controller: 'homeController'
                    }
                }
            })

            // Preferences
            .state('preferences', {
                url: '/voorkeuren',
                data: {
                    title: 'Voorkeuren'
                },
                views: {
                    '': {
                        templateUrl: 'app/components/preferences/preferences.html',
                        controller: 'preferencesController'
                    }
                }
            })

            // Products
            .state('products', {
                url: '/producten',
                data: {
                    title: 'Producten'
                },
                views: {
                    '': {
                        templateUrl: 'app/components/products/products.html',
                        controller: 'productsController'
                    }
                }
            })
            
            // Orders
            .state('orders', {
                url: '/bestellingen',
                data: {
                    title: 'Orders'
                },
                views: {
                    '': {
                        templateUrl: 'app/components/orders/orders.html',
                        controller: 'ordersController'
                    }
                }
            })
            
            // Frequentlyl asked questions
            .state('faq', {
                url: '/faq',
                data: {
                    title: 'Frequently asked questions'
                },
                views: {
                    '': {
                        templateUrl: 'app/components/faq/faq.html',
                    }
                }
            });

    }
}