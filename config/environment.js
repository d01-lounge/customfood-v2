var config = {};
	config.sessionSecret = 'd01-m3@tApp';
module.exports = function(){
	switch(process.env.NODE_ENV){
		case 'production':
			config.mongo = {
				db: 'custom-food-prod',
				url: 'mongodb://localhost/custom-food-prod'
			};
			return config;

		case 'testing':
			config.mongo = {
				db: 'custom-food-test',
				url: 'mongodb://localhost/custom-food-test'
			};
			return config;

		default:
			config.mongo = {
				db: 'custom-food-dev',
				url: 'mongodb://localhost/custom-food-dev'
			};
			return config;
	}
};