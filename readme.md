#Custom Food App v2.0

##Setup
Install npm depencies `npm install`.

##Run app
- Run `gulp dev`. App will (re)start in development mode.
- Run `gulp dev --env production` to start up in production mode.
- Run `gulp dev --env test` to start up in testing mode.