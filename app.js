var express = require('express'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	glob = require('glob'),
	fs = require('fs'),
	_ = require('lodash'),
	mongoose = require('mongoose'),
	session = require('express-session'),
	env = require('./config/environment.js'),
	swig = require('swig'),
	helmet = require('helmet'),
	fixtures = require('mongoose-fixtures'),
	models = glob('./models/**/*.js', {sync: true}),
	routes = glob('./routes/**/*.js', {sync: true});
env = new env();

var app = express();
mongoose.connect(env.mongo.url);

fixtures.load(__dirname + '/fixtures/');

// View engine setup
app.engine('html', swig.renderFile);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Get all models
_.forEach(models, function(model) {
	require(path.resolve(model));
});

// Get all routes
_.forEach(routes, function(route) {
	require('./routes/'+path.basename(route, path.extname(path.basename(route))))(app);
});

// Using helmet to make app more secure
app.use(helmet.xframe());
app.use(helmet.xssFilter());
app.use(helmet.nosniff());
app.use(helmet.ienoopen());
app.disable('x-powered-by');

// Start server
var server = app.listen(3000, function () {
	var host = server.address().address;
	var host = 'localhost';
	var port = server.address().port;
	console.log('Custom Food App listening at http://%s:%s running in %s mode.', host, port, [process.env.NODE_ENV || 'development']);
});

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// Error handlers
// Development error handler will print stacktrace
if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// Production error handler no stacktraces leaked to user
app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

module.exports = app;